// src/cli/prompt.js
import inquirer from 'inquirer';
import readline from 'readline';
import chalk from 'chalk';
import getChatCompletion from '../openai/chat.js';
import fetchModels from '../openai/models.js';
import {emitChatLog} from '../shared/utils/events.js';
import ora from 'ora'; // Import the ora package
import getStdin from 'get-stdin';

let selectedModel = '';
const conversationHistory = []; // Track conversation history

export async function nonInteractivePrompt(prompt, model, code) {
  // Read input from stdin
  const stdinData = await getStdin();

  // Combine the prompt and stdin data
  const separator = '_'.repeat(20);
  let fullPrompt = prompt;

  // Only add the separator lines and stdin data if stdin data exists
  if (stdinData.trim()) {
    fullPrompt += `\n${separator}\n\n${stdinData}${separator}`;
  }

  // Log the combined prompt to the console error stream
  process.stderr.write(`${chalk.cyan('\nYou:')} ${fullPrompt}\n`);

  // Get a response from the chat model
  const response = await getChatCompletion(fullPrompt, model);

  if (code) {
    // If requesting a code snippet, print only the AI's response to stdout
    console.log(response.trim());
  } else {
    // Log the response to the console error stream
    process.stderr.write(`${chalk.magenta('\nJarvis:')} ${response.replace(/\n/g, '\n')}\n`);
  }

  // Emit a log event with the full prompt and response
  emitChatLog(fullPrompt, response);
}

export async function interactivePrompt() {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.question(chalk.cyan('\nYou: '), async (input) => {
    // Create and start the spinner
    const spinner = ora('Loading response from Jarvis...').start();

    const response = await getChatCompletion(input, selectedModel);

    // Stop the spinner
    spinner.stop();

    console.log(chalk.magenta(`Jarvis: `) + response);

    emitChatLog(input, response);

    rl.close();

    // Track each interaction in the conversation history
    conversationHistory.push({
      question: input,
      answer: response,
    });

    // Recursive call for the next question
    await interactivePrompt();
  });

  // At the end of each call, return the conversation history
  return conversationHistory;
}

export async function selectModel() {
  try {
    const models = await fetchModels();
    const modelAnswer = await inquirer.prompt([
      {
        type: 'list',
        name: 'model',
        message: '\nPlease select a model:',
        choices: models,
      },
    ]);
    selectedModel = modelAnswer.model;
    return Promise.resolve();
  } catch (error) {
    return Promise.reject(error);
  }
}
