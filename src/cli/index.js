// src/cli/index.js
import {program} from 'commander';
import getStdin from 'get-stdin';
import {nonInteractivePrompt, interactivePrompt, selectModel} from './prompt.js';
import {eventEmitter} from '../shared/utils/events.js';
import {logToFile} from '../shared/utils/logs.js';
import {setChatLogConfig, getConfig} from '../shared/utils/config.js';

export async function runCli() {
  program
      .version('1.0.0')
      .description('Jarvis - Your Command Line AI Companion')
      .option('-i, --interactive', 'interactive mode', false)
      .option('-m, --model <model>', 'the model to use in non-interactive mode', 'gpt-3.5-turbo')
      .option('-c, --code [code]', 'request a code snippet')
      .option('-l, --language [language]', 'the language for the code snippet', 'python')
      .option('--enable-chat-log', 'enable chat logging')
      .option('--disable-chat-log', 'disable chat logging')
      .option('--show-config', 'display the current configuration') // New option
      .arguments('[prompt]')
      .action((prompt, options) => {
        if (options.code) {
          program.prompt = `Write a ${options.language} code snippet to: ${options.code}, Only respond with code as plain text without code block syntax around it.`;
        } else {
          program.prompt = prompt || 'What is the meaning of this!?';
        }
      })
      .parse(process.argv);

  const options = program.opts();

  // If --show-config option is set, log the config and exit
  if (options.showConfig) {
    const config = getConfig();
    console.log(config);
    process.exit(1);
  }

  // Enable or disable logging based on the command line parameters
  if (options.enableChatLog) {
    await setChatLogConfig(true);
    process.exit(1);
  }

  if (options.disableChatLog) {
    await setChatLogConfig(false);
    process.exit(1);
  }

  eventEmitter.on('interaction', (logEntry) => {
    logToFile(logEntry, 'chat_history', 'json');
  });

  if (!options.interactive && program.prompt === undefined) {
    // If in non-interactive mode and no argument was provided, read from stdin
    program.prompt = await getStdin();
  }

  if (!options.interactive) {
    if (options.model) {
      nonInteractivePrompt(program.prompt, options.model, options.code).then((conversationHistory) => {
      });
    } else {
      console.error('Model must be provided in non-interactive mode');
    }
  } else {
    selectModel().then(() => {
      interactivePrompt().then((conversationHistory) => {
        if (!options.interactive) {
          // Only print to console if non-interactive mode
          console.log(conversationHistory);
        }
      });
    }).catch((error) => {
      console.error(`Failed to select a model: ${error}`);
    });
  }
}

