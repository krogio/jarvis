// shared/utils/events.js
import EventEmitter from 'events';
export const eventEmitter = new EventEmitter();
import {v4 as uuidv4} from 'uuid';

const sessionId = uuidv4(); // Assign a unique session identifier when the script is loaded

export function emitChatLog(question, answer) {
  // Create a log entry for the current interaction
  const logEntry = {
    timestamp: new Date().toISOString(), // This will log the current date and time in the ISO format
    sessionId: sessionId,
    question: question,
    answer: answer,
  };

  // Emit an event with the log entry
  eventEmitter.emit('interaction', {logEntry, format: 'json'});
}
