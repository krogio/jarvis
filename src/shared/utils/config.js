// shared/utils/config.js
import fs from 'fs';
import path, {dirname} from 'path';
import {fileURLToPath} from 'url';
import readlineSync from 'readline-sync';
import ora from 'ora'; // Import ora

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const CONFIG_FILE_PATH = path.resolve(__dirname, '../../config.json');

export function getConfig() {
  if (fs.existsSync(CONFIG_FILE_PATH)) {
    const rawData = fs.readFileSync(CONFIG_FILE_PATH);
    return JSON.parse(rawData);
  } else {
    throw new Error('Configuration file not found');
  }
}

export function ensureConfig() {
  let config;
  let isConfigModified = false; // This flag tracks if we've changed the config

  if (fs.existsSync(CONFIG_FILE_PATH)) {
    config = JSON.parse(fs.readFileSync(CONFIG_FILE_PATH));
  } else {
    config = {};
  }

  if (!config.OPENAI_API_KEY) {
    config.OPENAI_API_KEY = readlineSync.question('Please enter your OpenAI API key: ', {
      hideEchoBack: true,
    });
    isConfigModified = true;
  }

  if (config.LOGGING_ENABLED === undefined) {
    const isLoggingEnabled = readlineSync.question('Would you like to enable logging? (yes(y) or no(n)): ', {
      limit: ['yes', 'no', 'y', 'n'],
      limitMessage: 'Please enter "yes", "no", "y", or "n"',
    });
    config.LOGGING_ENABLED = (isLoggingEnabled === 'yes' || isLoggingEnabled === 'y');
    isConfigModified = true;
  }

  // Only write to the file if we've made changes
  if (isConfigModified) {
    fs.writeFileSync(CONFIG_FILE_PATH, JSON.stringify(config, null, 2));
  }

  return config;
}

export function setConfig(newConfig) {
  const config = getConfig();
  const updatedConfig = {...config, ...newConfig};

  fs.writeFileSync(CONFIG_FILE_PATH, JSON.stringify(updatedConfig, null, 2));
}

const spinner = ora(); // Initialize ora spinner

export async function setChatLogConfig(enable) {
  try {
    const action = enable ? 'Enabling' : 'Disabling';
    spinner.start(`${action} Chat Log...`);
    await setConfig({LOGGING_ENABLED: enable});
    spinner.succeed(`Chat Log ${enable ? 'Enabled' : 'Disabled'} Successfully`);
  } catch (error) {
    spinner.fail(`Failed to ${enable ? 'Enable' : 'Disable'} Chat Log`);
    console.error(`Error: ${error}`);
    process.exit(1);
  }
}


