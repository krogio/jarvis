import fs from 'fs';
import path from 'path';
import moment from 'moment'; // Import moment.js for date manipulation
import {getConfig} from './config.js'; // Make sure this path is correct
import {fileURLToPath} from 'url'; // Import this for use with __dirname

// Define __dirname in ES6 module
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export function logToFile(data, type = 'chat_history', format = 'json') {
  const config = getConfig(); // Fetch configuration data

  // If logging is not enabled, return without doing anything
  if (!config.LOGGING_ENABLED) {
    return;
  }

  // Define a log directory and file based on the current date
  const logFileName = `chat_${moment().format('YYYY_MM_DD')}.log`;

  // The type now corresponds to a subdirectory within the main log folder
  // Use __dirname for an absolute path
  const logFolder = path.join(__dirname, '..', 'logs', type);
  const filePath = path.join(logFolder, logFileName);

  const formattedData = formatData(data, format);

  // Ensure the log directory exists
  if (!fs.existsSync(logFolder)) {
    fs.mkdirSync(logFolder, {recursive: true});
  }

  // Append to the log file for the current day
  fs.appendFileSync(filePath, formattedData + '\n');
}

function formatData(data, format) {
  if (format === 'json') {
    return JSON.stringify(data);
  } else if (format === 'txt') {
    return data.toString();
  } else {
    throw new Error(`Unsupported format: ${format}`);
  }
}
