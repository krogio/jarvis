// src/openai/models.js
import axios from 'axios';
import configuration from './config.js';

async function fetchModels() {
  try {
    const response = await axios.get('https://api.openai.com/v1/models', {
      headers: {
        'Authorization': `Bearer ${configuration.apiKey}`,
      },
    });

    const models = response.data.data;
    const sortedModels = models
        .filter((model) => model.id.startsWith('gpt-'))
        .sort((a, b) => new Date(b.created) - new Date(a.created))
        .map((model) => model.id);

    return sortedModels;
  } catch (error) {
    console.error(`Failed to fetch models: ${error}`);
    return [];
  }
}

export default fetchModels;
