// src/openai/chat.js
import {OpenAIApi} from 'openai';
import configuration from './config.js';

const openai = new OpenAIApi(configuration);
const conversationHistory = [];

async function getChatCompletion(question, selectedModel) {
  conversationHistory.push({role: 'user', content: question});
  try {
    const chatCompletion = await openai.createChatCompletion({
      model: selectedModel,
      messages: conversationHistory,
    });

    const response = chatCompletion.data.choices[0].message.content;

    conversationHistory.push({role: 'assistant', content: response});
    return response;
  } catch (err) {
    console.error(err);
  }
}

export default getChatCompletion;
