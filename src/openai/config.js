// src/openai/config.js
import {Configuration} from 'openai';
import {ensureConfig} from '../shared/utils/config.js';

// Fetch configuration data
const config = ensureConfig();

const configuration = new Configuration({
  apiKey: config.OPENAI_API_KEY,
});

export default configuration;
