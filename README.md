<img src="./src/shared/assets/jarvis_logo.jpg" alt="Jarvis Logo" width="80" height="80">

# Jarvis - Your Command Line AI Companion
Jarvis is a command-line application that enables you to converse with OpenAI's advanced language models. You can use it interactively or pass a prompt directly. Additionally, Jarvis can generate code snippets in various programming languages.

## Support
If you find this project useful, please consider supporting our maintainers - [buy us some coffee](https://www.buymeacoffee.com/krogio)

<img src="./src/shared/assets/bmc_qr.png" alt="Buy Me Coffee QR Code" width="80" height="80">

## Usage
Once you've set up the application, you can initiate a conversation by running the application and following the prompts. You can pass several options to modify the behavior of the program:

- `-i, --interactive` to launch the program in interactive mode.
- `-m, --model <model>` to specify the model to use (only for non-interactive mode).
- `-c, --code <code>` to request a code snippet.
- `-l, --language <language>` to specify the language for the code snippet. Default is "python".
- `--enable-chat-log` to enable chat logging.
- `--disable-chat-log` to disable chat logging.
- `--show-config` to display the current configuration.

For example, you can request a Python code snippet that prints "hello world" like this:

```bash
jarvis -l "python" -c "hello world"
```

You can even pipe the output directly to a file:
```bash
jarvis -l "python" -c "hello world" >> hello_world.py
```

## Getting Started
These instructions will guide you in setting up a copy of the project on your local machine.

### Prerequisites
- [Node.js](https://nodejs.org/en/download/)
- An OpenAI API key. You can obtain this from the [OpenAI Platform Signup](https://platform.openai.com/signup/)
- Alternatively we can issue an OpenAI API Key upon request. Please email [jarvis@krog.io](mailto:jarvis@krog.io) for more information.

### Installation
1. **Clone the repository:**
    ```bash
    git clone https://gitlab.com/krogio/jarvis
    cd jarvis
    ```
2. **Install the dependencies:**
    ```bash
    npm install
    ```
3. **Run the application:**
    ```bash
    npm start
    ```
    - On first run, you'll be prompted to enter your OpenAI API Key. Please enter the key you received from OpenAI.

## Create an Alias:

For Unix-like operating systems (e.g., Linux, MacOS):

1. Open your shell configuration file (e.g., `~/.bashrc`, `~/.bash_profile`, or `~/.zshrc`).
2. Add the following line:
    ```bash
    alias jarvis="node /path/to/your/project/jarvis/src/index.js"
    ```
    Replace `/path/to/your/project/jarvis` with the actual path to the `jarvis` directory on your system.
3. After adding this line, you'll need to either source your shell configuration file or restart your terminal session.

For Windows:

1. Open a new cmd window as Administrator
2. Enter the following command:
    ```cmd
    doskey jarvis=node /path/to/your/project/jarvis/src/index.js
    ```
    Replace `/path/to/your/project/jarvis` with the actual path to the `jarvis` directory on your system.

Now, you can use the `jarvis` command to run the application from any location in your terminal.

## Built With
- [Node.js](https://nodejs.org/)
- [Axios](https://axios-http.com/)
- [Inquirer](https://www.npmjs.com/package/inquirer)
- [OpenAI API](https://openai.com/)

## Contributing
We welcome contributions from everyone. Here are some ways you can contribute:

Submitting bug reports or feature requests.
Improving the documentation.
Fixing existing issues or adding new features.
Before making a major change, please first discuss the change you wish to make via an issue.

## License
This project is open source under the terms of the [MIT License](LICENSE.md).
